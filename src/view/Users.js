import React, { Component } from 'react'
import { Link } from "react-router-dom"
import { connect } from "react-redux"


class Users extends Component {

  render() {

    return (
      <div>
        <h1>Users</h1>
        <ul>
          {
            this.props.users.map((user) =>
              <li key={user.id}>
                <Link to={`/user/${user.id}`} key={user.id}> {user.name}
                  {this.props.posts.filter(post =>
                    +post.userId === +user.id
                  ).length
                  }
                </Link>
              </li>)

          }
        </ul>
      </div>
    )
  }
}

export default connect(
  state => ( {
    users: state.users,
    posts: state.posts
  } ),
)(Users)
