import React, { Component } from 'react'
import { connect } from "react-redux"
import { Link } from "react-router-dom"

class Posts extends Component {

  render() {

    return (
      <div>
        <h1>Posts</h1>
        <ul>

          {
            this.props.posts.filter(post =>
              +post.userId === +this.props.match.params.id
            ).map((post) =>
              <li key={post.id}>
                <Link to={`/post/${post.id}`}>
                  {post.body}
                </Link>
              </li>)
          }

          <Link to='/'>Home</Link>

        </ul>
      </div>
    )
  }
}

export default connect(
  state => ( {
    posts: state.posts
  } ),
)(Posts)
