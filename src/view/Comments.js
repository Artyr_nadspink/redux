import React, { Component } from 'react'
import { Link } from "react-router-dom"
import { getInfo } from "../lib/request"

class Comments extends Component {
  constructor(props) {
    super(props)
    this.state = {
      comments: []
    }
  }

  componentDidMount = async () => {
    try {
      const comments = await getInfo('https://jsonplaceholder.typicode.com/comments/')
      this.setState({ comments: comments })
    }
    catch (e) {
      console.error('Couldn\'t get comments: ' + e)
    }
  }

  render() {

    return (
      <div>
        Comments
        <ul>
          {
            this.state.comments.filter(comment =>
              +comment.postId === +this.props.match.params.id
            ).map((comment) =>
              <li key={comment.id}>
                {comment.body}

              </li>)
          }
          <Link to='/'>Home</Link>
        </ul>
      </div>
    )
  }
}


export default Comments
