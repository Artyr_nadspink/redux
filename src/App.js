import React, { Component } from 'react'
import './App.css'
import { Route, Switch, withRouter } from "react-router-dom"
import Users from "./view/Users"
import Posts from "./view/Posts"
import Comments from "./view/Comments"
import { getInfo } from "./lib/request"
import { connect } from 'react-redux'
import  {addUsers,addPosts} from "./actions"

class App extends Component {

  componentDidMount = async () => {
    try {
      const users = await getInfo('https://jsonplaceholder.typicode.com/users/')
      const posts = await getInfo('https://jsonplaceholder.typicode.com/posts/')
      this.props.addUsers(users)
      this.props.addPosts(posts)
    }
    catch (e) {
      console.error('Couldn\'t : ' + e)
    }
  }

  render() {
    return (
      <div className="App">
        <Switch>
          <Route exact path='/' component={Users}/>
          <Route exact path='/user/:id' component={Posts}/>
          <Route exact path='/post/:id' component={Comments}/>
        </Switch>

      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  users: state.users,
  posts: state.posts
})

const mapDispatchToProps = {
  addUsers,
  addPosts,
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App))



