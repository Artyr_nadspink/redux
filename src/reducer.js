const initialState = {
  users: [],
  posts: []
}
export default (state = initialState, action) => {
  switch (action.type) {
  case 'ADD_USERS':
    return {
      ...state,
      users: action.payload.slice()
    }
  case 'ADD_POSTS':
    return {
      ...state,
      posts: action.payload.slice()
    }
  default:
    return state
  }
}