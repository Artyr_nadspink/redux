import axios from "axios/index"

export const getInfo = async (url) => {
  return axios.get(url)
    .then((res) => res.data)
}